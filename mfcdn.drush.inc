<?php

/**
 * @file
 * Image module's drush integration.
 *
 * @todo image-build($field_name, $bundle, $style_name)
 */

/**
 * Implements hook_drush_command().
 */
function mfcdn_drush_command() {
  $items = array();
  if (!function_exists('module_load_include')) {
    include_once(DRUPAL_ROOT . '/includes/module.inc');
  }
  if (!module_load_include('inc', 'mfcdn', 'mfcdn')) {
    include_once(dirname(__FILE__) . '/mfcdn.inc');
  }

  $items['mfcdn-images'] = array(
    'callback' => 'drush_mfcdn_images',
    'description' => dt('Copy all derived images in use to @dir.', array(
      '@dir' => mfcdn_get_filesystem_paths('image/'),
    )),
    'core' => array('7+'),
    'drupal_dependencies' => array('image'),
    'arguments' => array(
      'fid_start' => 'fid starting number.',
      'fid_end' => 'fid ending number.',
    ),
    'examples' => array(
      'drush mirror-cdn-images' => 'Copy all image derivatives to the CDN.',
    ),
  );
  $items['mfcdn-css-js'] = array(
    'callback' => 'drush_mfcdn_css_js',
    'description' => dt('Copy all CSS aggregates to @css_dir & all JS aggregates to @js_dir.', array(
      '@css_dir' => mfcdn_get_filesystem_paths('text/css'),
      '@js_dir' => mfcdn_get_filesystem_paths('application/javascript'),
    )),
    'core' => array('7+'),
    'drupal_dependencies' => array('advagg'),
    'examples' => array(
      'drush mirror-cdn-css-js' => 'Copy all CSS/JS aggregates to the CDN.',
    ),
  );
  return $items;
}

/**
 * Implements hook_drush_help().
 */
function mfcdn_drush_help($command) {
  module_load_include('inc', 'mfcdn', 'mfcdn');

  switch ($command) {
    case 'drush:mirror-cdn-images':
      return dt('Copy all derived images in use to @dir.', array('@dir' => mfcdn_get_filesystem_paths('image/')));

    case 'drush:mirror-cdn-css-js':
      return dt('Copy all CSS aggregates to @css_dir & all JS aggregates to @js_dir.', array(
        '@css_dir' => mfcdn_get_filesystem_paths('text/css'),
        '@js_dir' => mfcdn_get_filesystem_paths('application/javascript'),
      ));
  }
}

/**
 * Drush callback.
 */
function drush_mfcdn_css_js() {
  module_load_include('inc', 'mfcdn', 'mfcdn');
  list($css_path, $js_path) = advagg_get_root_files_dir();

  // Find all files in the advagg CSS/JS directories and copy them.
  $css_files = file_scan_directory($css_path[0], '/.*/');
  $js_files = file_scan_directory($js_path[0], '/.*/');
  $target_css = mfcdn_get_filesystem_paths('text/css');
  $target_js = mfcdn_get_filesystem_paths('application/javascript');

  drush_print(dt("Number of CSS files: @css_count. \nNumber of JS files: @js_count. \nCSS target directory: @css_dir. \nJS traget directory: @js_dir.", array(
    '@css_count' => count($css_files),
    '@js_count' => count($js_files),
    '@css_dir' => $target_css,
    '@js_dir' => $target_js,
  )));

  file_prepare_directory($target_css, FILE_CREATE_DIRECTORY);
  $advagg_css_dir = $target_css . '/advagg_css';
  file_prepare_directory($advagg_css_dir, FILE_CREATE_DIRECTORY);
  list($uid, $gid, $dir_mode, $file_mode) = mfcdn_get_uid_gid_mode();
  mfcdn_chmodowngrp($target_css, $uid, $gid, $dir_mode, $file_mode);
  mfcdn_chmodowngrp($advagg_css_dir, $uid, $gid, $dir_mode, $file_mode);
  if ($target_css != $target_js) {
    file_prepare_directory($target_js, FILE_CREATE_DIRECTORY);
    mfcdn_chmodowngrp($target_js, $uid, $gid, $dir_mode, $file_mode);
  }
  $advagg_js_dir = $target_js . '/advagg_js';
  file_prepare_directory($advagg_js_dir, FILE_CREATE_DIRECTORY);
  mfcdn_chmodowngrp($advagg_js_dir, $uid, $gid, $dir_mode, $file_mode);

  mfcdn_copy_files_to_mirrors('text/css', array_keys($css_files));
  drush_log(dt("All CSS aggregates have been copied."));
  drush_print(dt("All CSS aggregates have been copied."));

  mfcdn_copy_files_to_mirrors('application/javascript', array_keys($js_files));
  drush_log(dt("All JS aggregates have been copied."));
  drush_print(dt("All JS aggregates have been copied."));

  mfcdn_recursive_chmodowngrp($target_css);
  if ($target_css != $target_js) {
    mfcdn_recursive_chmodowngrp($target_js);
  }
  drush_log(dt("File permissions and ownership has been set."));
  drush_print(dt("File permissions and ownership has been set."));
}

/**
 * Drush callback.
 */
function drush_mfcdn_images($fid_start = 0, $fid_end = 0) {
  module_load_include('inc', 'mfcdn', 'mfcdn');

  // Get first and last fid.
  $result = db_query("
    SELECT (
      SELECT file_managed.fid
      FROM file_managed AS file_managed
      INNER JOIN file_usage AS file_usage
        ON file_managed.fid = file_usage.fid
      WHERE file_managed.status = 1
      AND file_managed.filemime LIKE 'image/%'
      ORDER BY file_managed.fid ASC
      LIMIT 0, 1
    ) AS first_fid,
    (
      SELECT file_managed.fid
      FROM file_managed AS file_managed
      INNER JOIN file_usage AS file_usage
        ON file_managed.fid = file_usage.fid
      WHERE file_managed.status = 1
      AND file_managed.filemime LIKE 'image/%'
      ORDER BY file_managed.fid DESC
      LIMIT 0, 1
    ) AS last_fid
  ");
  $record = $result->fetchAssoc();

  if (empty($fid_start)) {
    $fid_start = $record['first_fid'];
  }
  if (empty($fid_end)) {
    $fid_end = $record['last_fid'];
  }

  // Get all images in use.
  $query = db_select('file_managed', 'file_managed')
    ->fields('file_managed', array('fid', 'uri'))
    ->condition('file_managed.status', 1)
    ->condition('file_managed.filemime', db_like('image/') . '%', 'LIKE')
    ->condition('file_managed.fid', array($fid_start, $fid_end), 'BETWEEN');
  $query->join('file_usage','file_usage','file_managed.fid = file_usage.fid');
  $results = $query->execute();
  $record['total_fid'] = $results->rowCount();
  $dir = mfcdn_get_filesystem_paths('image/');

  drush_print(dt("First fid in DB: @first_fid. \nLast fid in DB: @last_fid. \nTotal number of fids: @total_fid. \nTarget Directory: @dir", array(
    '@first_fid' => $fid_start,
    '@last_fid' => $fid_end,
    '@total_fid' => $record['total_fid'],
    '@dir' => $dir,
  )));

  $keyed_results = $results->fetchAllKeyed();
  $chunks = array_chunk($keyed_results, 5000, TRUE);

  file_prepare_directory($dir, FILE_CREATE_DIRECTORY);
  $image_styles_dir = $dir . '/styles';
  file_prepare_directory($image_styles_dir, FILE_CREATE_DIRECTORY);
  list($uid, $gid, $dir_mode, $file_mode) = mfcdn_get_uid_gid_mode();
  mfcdn_chmodowngrp($dir, $uid, $gid, $dir_mode, $file_mode);
  mfcdn_chmodowngrp($image_styles_dir, $uid, $gid, $dir_mode, $file_mode);

  foreach ($chunks as $files) {
    reset($files);
    $start = key($files);
    end($files);
    $end = key($files);
    reset($files);

    drush_print(dt("Copying fids range: @start - @end.", array(
      '@start' => $start,
      '@end' => $end,
    )));
    // Get derivative images.
    foreach ($files as $file) {
      $paths = mfcdn_get_image_style_paths($file);
      mfcdn_copy_files_to_mirrors('image/', $paths);
    }
  }
  drush_log(dt("All fids have been copied: @start - @end.", array(
    '@start' => $start,
    '@end' => $end,
  )));
  mfcdn_recursive_chmodowngrp($dir . '/styles');
  drush_log(dt("File permissions and ownership has been set."));
  drush_print(dt("File permissions and ownership has been set."));
}

function mfcdn_chmodowngrp($uri, $uid, $gid, $dir_mode, $file_mode) {
  if (is_dir($uri)) {
    $mode = $dir_mode;
  }
  else {
    $mode = $file_mode;
  }
  @chmod($uri, $mode);
  if (!is_null($uid)) {
    @chown($uri, $uid);
  }
  if (!is_null($gid)) {
    @chgrp($uri, $gid);
  }
}

function mfcdn_recursive_chmodowngrp($dir, $uid = NULL, $gid = NULL, $dir_mode = NULL, $file_mode = NULL) {
  list($uid, $gid, $dir_mode, $file_mode) = mfcdn_get_uid_gid_mode($uid, $gid, $dir_mode, $file_mode);
  // Do the top level dir first.
  mfcdn_chmodowngrp($dir, $uid, $gid, $dir_mode, $file_mode);

  // Do recursive chmod, chown, chgrp.
  if ($files = file_scan_directory($dir, "/.*/")) {
    foreach ($files as $file) {
      mfcdn_chmodowngrp($file->uri, $uid, $gid, $dir_mode, $file_mode);
      if (is_dir($file->uri)) {
        mfcdn_recursive_chmodowngrp($file->uri, $uid, $gid, $dir_mode, $file_mode);
      }
    }
  }
}

function mfcdn_get_uid_gid_mode($uid = NULL, $gid = NULL, $dir_mode = NULL, $file_mode = NULL) {
  // Get public UID
  if (is_null($uid)) {
    if (empty($stat_public)) {
      $stat_public = stat('public://');
    }
    if (isset($stat_public['uid'])) {
      $uid = $stat_public['uid'];
    }
  }

  // Get publis GID
  if (isset($stat_public['gid']) && is_null($gid)) {
    if (empty($stat_public)) {
      $stat_public = stat('public://');
    }
    if (isset($stat_public['gid'])) {
      $gid = $stat_public['gid'];
    }
  }

  // Get chmod numbers.
  if (is_null($dir_mode)) {
    $dir_mode = variable_get('file_chmod_directory', 0775);
  }
  if (is_null($file_mode)) {
    $file_mode = variable_get('file_chmod_file', 0664);
  }
  return array($uid, $gid, $dir_mode, $file_mode);
}
