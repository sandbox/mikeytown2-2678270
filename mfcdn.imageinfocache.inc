<?php

/**
 * @file
 * Mirror Files CDN module.
 *
 * File used to store hook_imageinfo_cache_* hooks.
 */

/**
 * Implements hook_image_imageinfo_cache_save().
 */
function mfcdn_image_imageinfo_cache_save($image, $destination, $return) {
  if (!empty($return) && !empty($destination)) {
    module_load_include('inc', 'mfcdn', 'mfcdn');
    $mime = isset($image->info['mime_type']) ? $image->info['mime_type'] : '';
    mfcdn_copy_files_to_mirrors($mime, array($destination));
  }
}
