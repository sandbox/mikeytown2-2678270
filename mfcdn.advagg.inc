<?php

/**
 * @file
 * Mirror Files CDN module.
 *
 * File used to store hook_advagg_* hooks.
 */

/**
 * Implements hook_advagg_save_aggregate_alter().
 */
function mfcdn_advagg_save_aggregate_alter($files_to_save, $aggregate_settings, $other_parameters) {
  $filenames = array_keys($files_to_save);

  // Get mime.
  $filetype = '';
  if (!empty($other_parameters[1]) && ($other_parameters[1] == 'css' || $other_parameters[1] == 'js')) {
    $filetype = $other_parameters[1];
  }
  else {
    foreach ($filenames as $filename) {
      $filetype = mfcdn_advagg_get_filetype_from_filename($filename);
      if (!empty($filetype)) {
        break;
      }
    }
  }
  $mime = mfcdn_advagg_get_mime_from_filetype($filetype);

  module_load_include('inc', 'mfcdn', 'mfcdn');
  // Use a shutdown function so the file exists.
  drupal_register_shutdown_function('mfcdn_copy_files_to_mirrors', $mime, $filenames);
}

/**
 * Implements hook_advagg_removed_aggregates().
 */
function mfcdn_advagg_removed_aggregates($kill_list) {
  module_load_include('inc', 'mfcdn', 'mfcdn');
  $file_list = array();
  foreach ($kill_list as $filename) {
    $filetype = mfcdn_advagg_get_filetype_from_filename($filename);
    $mime = mfcdn_advagg_get_mime_from_filetype($filetype);
    $file_list[$mime] = $filename;
  }
  foreach ($file_list as $mime => $files) {
    mfcdn_delete_files_from_mirrors($mime, $files);
  }
}

/**
 * Given a filename, get the file type.
 *
 * @param string $filename
 *   URI of file.
 *
 * @return string
 *   css, js, or an empty string.
 */
function mfcdn_advagg_get_filetype_from_filename($filename) {
  if (empty($filename)) {
    return '';
  }

  $file_extensions = array(
    '.js' => 'js',
    '.css' => 'css',
    '.js.gz' => 'js',
    '.css.gz' => 'css',
  );
  $reversed_filename = strrev($filename);
  foreach ($file_extensions as $ext => $type) {
    if (stripos($reversed_filename, strrev($ext)) === 0) {
      return $type;
    }
  }
  return '';
}

/**
 * Given a filetype get the mime.
 *
 * @param string $filetype
 *   css or js.
 *
 * @return string
 *   text/css, application/javascript, or an empty string.
 */
function mfcdn_advagg_get_mime_from_filetype($filetype) {
  if (empty($filetype)) {
    return '';
  }

  if (!empty($filetype) && is_string($filetype)) {
    if ($filetype == 'css') {
      return 'text/css';
    }
    elseif ($filetype == 'js') {
      return 'application/javascript';
    }
  }
  return '';
}
