<?php

/**
 * @file
 * Mirror Files CDN module.
 *
 * Do file operations.
 */

/**
 * Given a path gets all image style derivatives.
 *
 * @param string $path
 *   The Drupal file path to the original image.
 *
 * @return array
 *   An array of image style derivatives based off the input path.
 */
function mfcdn_get_image_style_paths($path) {
  $paths = array();
  $styles = image_styles();
  foreach ($styles as $style) {
    $image_path = image_style_path($style['name'], $path);
    if (file_exists($image_path)) {
      $paths[] = $image_path;
    }
  }
  return $paths;
}

function mfcdn_get_filesystem_paths($mime) {
  if ($mime == 'text/css') {
    $return = variable_get('mfcdn_css', MFCDN_CSS);
  }
  elseif ($mime == 'application/javascript') {
    $return = variable_get('mfcdn_js', MFCDN_JS);
  }
  elseif (strpos($mime, 'image/') === 0) {
    $return = variable_get('mfcdn_image', MFCDN_IMAGE);
  }

  if (empty($return)) {
    $return = variable_get('mfcdn_fallback', MFCDN_FALLBACK);
  }
  return $return;
}

function mfcdn_copy_files_to_mirrors($mime, $files) {
  $fs_paths = mfcdn_get_filesystem_paths($mime);
  if (empty($fs_paths)) {
    return;
  }

  foreach ($files as $file) {
    // If the source file doesn't exist, skip it.
    if (!file_exists($file)) {
      continue;
    }

    $target = str_replace('public://', $fs_paths . '/', $file);
    $dir = drupal_dirname($target);
    file_prepare_directory($dir, FILE_CREATE_DIRECTORY);
    file_unmanaged_copy($file, $target, FILE_EXISTS_REPLACE);
  }

//   watchdog('mfcdn', 'copy ' . httprl_pr($mime, $files, $fs_paths, $dir));
}

function mfcdn_delete_files_from_mirrors($mime, $files) {
  $fs_paths = mfcdn_get_filesystem_paths($mime);
  if (empty($fs_paths)) {
    return;
  }

  foreach ($files as $file) {
    $target = str_replace('public://', $fs_paths . '/', $file);
    if (file_exists($target)) {
      file_unmanaged_delete($target);
    }
  }

//   watchdog('mfcdn', 'delete ' . httprl_pr($mime, $files, $fs_paths));
}
