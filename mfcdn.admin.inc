<?php

/**
 * @file
 * Admin page callbacks for the mirror file cdn module.
 */

/**
 * Form builder; Configure mfcdn settings.
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function mfcdn_admin_settings_form($form, $form_state) {
  drupal_set_title(t('MFCDN: Configuration'));

  $form['mirror_locations'] = array(
    '#type' => 'fieldset',
    '#title' => t('Mirror Locations'),
  );
  $form['mirror_locations']['mfcdn_css'] = array(
    '#type' => 'textfield',
    '#title' => t('CSS mirror location'),
    '#default_value' => variable_get('mfcdn_css', MFCDN_CSS),
    '#access' => FALSE,
  );
  $form['mirror_locations']['mfcdn_js'] = array(
    '#type' => 'textfield',
    '#title' => t('JavaScript mirror location'),
    '#default_value' => variable_get('mfcdn_js', MFCDN_JS),
    '#access' => FALSE,
  );
  $form['mirror_locations']['mfcdn_image'] = array(
    '#type' => 'textfield',
    '#title' => t('Image mirror location'),
    '#default_value' => variable_get('mfcdn_image', MFCDN_IMAGE),
    '#access' => FALSE,
  );
  $form['mirror_locations']['mfcdn_fallback'] = array(
    '#type' => 'textfield',
    '#title' => t('Target directory'),
    '#default_value' => variable_get('mfcdn_fallback', MFCDN_FALLBACK),
  );

  $form['#validate'][] = 'mfcdn_admin_settings_form_validate';
  return system_settings_form($form);
}

// Validate callback.
/**
 * Make sure the unified multisite directory was created correctly.
 */
function mfcdn_admin_settings_form_validate($form, &$form_state) {
  foreach (element_children($form['mirror_locations']) as $key) {
    $dir = rtrim($form_state['values'][$key], '/');
    // Return if unified_multisite_dir is not set.
    if (!empty($dir)) {
    // Prepare directory.
      if (!is_dir($dir) || !is_writable($dir)) {
        form_set_error($key, t('%dir is not a directory or can not be written to. The directory needs to have the same file system permissions as the "Public file system path" found on the <a href="@file_system_link">File System configuration page</a>.', array(
          '%dir' => $dir,
          '@file_system_link' => url('admin/config/media/file-system'),
        )));
      }
    }
  }
}
